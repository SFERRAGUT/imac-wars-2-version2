#include <string.h>
#include <stdio.h>
#include "ia.h"
#include <iostream>

using namespace std;

extern bool action_buy;
extern int unitCount;
extern int unitCount_IA;
extern Unit* allUnits;
extern Unit* allUnits_IA;
extern int currentUnit;
extern Map mapTerrain;
extern list<point>chemin;

int currentAchat = -1 ;

void init_IA(int currentPlayer){ //Ach�te les premi�res unit de l'IA au premier tour : une de chaque
    cout <<"Init ia"<< endl;
    buyUnit_IA(currentPlayer, 2);
    buyUnit_IA(currentPlayer, 1);
    buyUnit_IA(currentPlayer, 0);
    changePlayer(&allUnits[currentUnit], allUnits);
    return;
}

void game_IA(int currentPlayer){
    cout << "L'IA joue (" << currentPlayer << ")..." << endl;

    if (money[1]>=3000){
        buyUnit_IA(currentPlayer, 2);
        currentAchat = 2;
        action_buy = true;
    }
    else if (money[1] >= 2000){
        buyUnit_IA(currentPlayer, 1);
        currentAchat = 1;
        action_buy = true;

    }
    else if (money[1] >= 1000){
        buyUnit_IA(currentPlayer, 0);
        currentAchat = 0;
        action_buy = true;
    }
    checkCible(currentPlayer);
}


void buyUnit_IA(int currentPlayer, int type){
    unitCount ++;
    unitCount_IA ++;
    allUnits = (Unit*) realloc(allUnits, unitCount*sizeof(Unit));
    allUnits_IA = (Unit*) realloc(allUnits_IA, unitCount_IA*sizeof(Unit));
    Unit unit = unit_init(type,currentPlayer);
    int PosX = unit.x;
    int PosY = unit.y;
    Position_unit(&unit, PosX, PosY);
    allUnits[unitCount-1] = unit;
    allUnits_IA[unitCount_IA-1] = unit;
    cout << "L'IA construit une unit�...qui appartient a " << unit.owner << "place en" << unit.x << " ; " << unit.y << endl;
}


void Position_unit(Unit* unit, int PosX, int PosY){
    cout << "Je cherche � le positionner" << endl;
    int terrain_herbe = 6;
    bool position_ok = true;

    if (unit->owner == 0){ //si l'unit� appartient au Player 1
        PosX = rand() % 5;
        PosY = rand() % 5;
        cout << "Position al�atoire : " << PosX<< " ; " << PosY << endl;
        for(int i=0; i<unitCount; i++){
            if(mapTerrain.cells[PosY][PosX] != terrain_herbe || (allUnits[i].x == PosX && allUnits[i].y == PosY)){
                    position_ok = false;
                    break;
                    cout << "Position impossible" << endl;
            }
        }
        if (position_ok){
            cout << "Position ok ! On place" << endl;
            unit->x = PosX;
            unit->y = PosY;
        }
        else{
            Position_unit(unit, PosX, PosY);
            position_ok = true;
        }
    }

    else if (unit->owner == 1){ //si l'unit� appartient au Player 2
        int PosX = (rand() % 4) + 10;
        int PosY = (rand() % 4) + 10;
        cout << "Position al�atoire : " << PosX<< " ; " << PosY << endl;

        for(int i=0; i<unitCount; i++){
            if(mapTerrain.cells[PosY][PosX] != terrain_herbe || (allUnits[i].x == PosX && allUnits[i].y == PosY)){
                    position_ok = false;
                    break;
                    cout << "Position impossible" << endl;
            }
        }
        if (position_ok){
            cout << "Position ok ! On place" << endl;
            unit->x = PosX;
            unit->y = PosY;

        }
        else{
            Position_unit(unit, PosX, PosY);
            position_ok = true;
        }
    }
}

void checkCible(int currentPlayer){
    int movingToAttack;
    distanceCible arrayDist[unitCount];
    bool attack = false;

    for (int j =0; j<unitCount; j++){
        //cout << "On rentre dans la boucle for 1" << endl;
        if (allUnits[j].owner == 1){ // SI l'unit� est � l'IA
            distanceCible minDistance;
            minDistance.aiIndex = j;
            for (int i =0; i<unitCount ; i++){
                // cout << "On rentre dans la boucle for 2" << endl;
                if (allUnits[i].owner == 0 || allUnits[i].type==5) {
                    // cout << "On rentre dans le if"<< endl;
                    int deltax = allUnits[i].x - allUnits[j].x;
                    int deltay = allUnits[i].y - allUnits[j].y;
                    if (abs(deltax) + abs(deltay) <= allUnits[j].range ){
                        cout << "Unit � port�e : " << j << " port�e : " << allUnits[j].range << " delta x = " << deltax << " deltay = " << deltay<< endl;
                        minDistance.inRange = true;
                        attack = true;
                    }
                    point caseInRange;
                    point departIa;
                    departIa.x = allUnits[j].x;
                    departIa.y = allUnits[j].y;
                    caseInRange.x = allUnits[j].x + deltax;
                    caseInRange.y = allUnits[j].y + deltay - allUnits[j].range;
                    for (int k = 0; k < unitCount ; k++){
                        // cout << "On rentre dans la boucle for 3, avec k = " <<k<< endl;
                        if (caseInRange.x == allUnits[k].x && caseInRange.y == allUnits[k].y){ //On v�rifie que la premi�re case th�orique pour �tre � port�e d'attaque est vide
                            cout <<"La caseInRange est prise, on la change " << endl;
                            if (deltax>0 && deltay >0){ //En fonction de la position relative des unit, pour �viter au max d'aller hors de la map
                                deltax --;
                                deltay ++;
                            }
                            else if (deltax <0 && deltay >0){
                                deltax ++;
                                deltay --;
                            }
                            else if (deltax<0 && deltay <0){
                                deltax ++;
                                deltay ++;
                            }
                            else if (deltax>0 && deltay <0){
                                deltax --;
                                deltay --;
                            }
                            else if (deltax==0 && deltay <0){
                                deltay ++;
                            }
                            else if (deltax==0 && deltay >0){
                                deltay --;
                            }
                            else if (deltax<0 && deltay==0){
                                deltax --;
                            }
                            else if (deltax>0 && deltay==0){
                                deltax ++;
                            }
                            caseInRange.x = allUnits[j].x + deltax;
                        }
                        if (allUnits[j].y + deltay < allUnits[j].range){
                            caseInRange.y = allUnits[j].y + deltay + allUnits[j].range;
                        }
                        else{
                            caseInRange.y = allUnits[j].y + deltay - allUnits[j].range;
                        }
                    }
                    // cout << "Avant de trouver un chemin"<< endl;
                    //cout << "caseInRange x = " << caseInRange.x << " ; y = " << caseInRange.y << endl;
                    pathFinder(departIa,caseInRange);
                    // cout << "Apr�s avoir trouv� un chemin" << endl;

                    if (chemin.size() < minDistance.shorterPath.size() || minDistance.shorterPath.empty()){
                        // cout << "affecte la dist min" << endl;
                        minDistance.shorterPath = chemin;
                        //cout << "Taille du parth de minDistance : " << minDistance.shorterPath.size()<<endl;
                        minDistance.playerIndex = i;
                    }
                }
            }

        cout << "unitCount = "<< unitCount << endl;
        // cout << "arrayDist[j].shorterPath.size() :  " << arrayDist[j].shorterPath.size() << endl;
        // cout << "Move : allUnits_IA[arrayDist[j].aiIndex].move " << allUnits_IA[arrayDist[j].aiIndex].move<< endl;
        cout << "Taille du chemin le plus court de l'unit "<<j<<" qui a une port�e de "<< allUnits[j].range << " = " << minDistance.shorterPath.size() << endl;
        if (allUnits[minDistance.aiIndex].move >= minDistance.shorterPath.size()){
            // cout << "Avant affectation canAttack"<< endl;
            cout << "Elle peut donc attaquer si elle se d�place"<< endl;
            minDistance.canAttack = true;
        }
        // cout << "Avant affectation miniDistance"<< endl;
        arrayDist[j] = minDistance;
        chemin = minDistance.shorterPath;
        }
    // cout << "All�luia, on est sorti du for 1 :)" << endl;
    }

    float maxPower = 0.;
    int nbUnitCanAttack = 0;
    int unitShortestPath = 0;
    int shortestPath = -1;
    int attackingUnit = -1;
    int maxDamage = 0;
    int test = 0;
    list<point> :: iterator it;

    for (int i =0; i<unitCount; i++){
        if (allUnits[i].owner == 1){
            if (attack){
            // cout << "On rentred dans le 1er if attack" << endl;
                cout << arrayDist[i].inRange << endl;
                if (arrayDist[i].inRange && allUnits[attackingUnit].power*allUnits[attackingUnit].pv > maxDamage || maxDamage ==0){ //Onchoisit l'unit qui tape le plus fort
                    // cout << "On est dans le if" << endl;
                    cout << "On trie qui tape le plus fort..."<< endl;
                    maxDamage = allUnits[i].power*allUnits[i].pv;
                    attackingUnit = i;
                }
            }
            else{
                if (arrayDist[i].canAttack == true){
                    if (allUnits[i].power>maxPower){
                        maxPower = allUnits[i].power;
                        movingToAttack=i;
                    }
                    nbUnitCanAttack ++;
                }
                if (arrayDist[i].shorterPath.size() < shortestPath || shortestPath == -1 ){
                    shortestPath = arrayDist[i].shorterPath.size();
                    unitShortestPath = i;
                }
            }
        }
    }
    if (attackingUnit != -1){
        cout << "C'est l'unit� " << attackingUnit << " qui va attaquer, de type " << allUnits[attackingUnit].type<< endl;
    }
    cout<<"Choix de L'IA : " << endl;

    if (attack){
        cout << "Attaque avec unit = "<< attackingUnit << " de type : "<< allUnits[attackingUnit].type <<endl;
        cout << "position unit de l'IA s�lectionn�e = "<< allUnits[attackingUnit].x << " ; " << allUnits[attackingUnit].y << endl;
        cout << "position unit cible x = "<<allUnits[arrayDist[attackingUnit].playerIndex].x<< " position unit cible y = " << allUnits[arrayDist[attackingUnit].playerIndex].y<<endl;
        currentUnit = attackingUnit;
        action(&allUnits[currentUnit], allUnits[arrayDist[attackingUnit].playerIndex].x, allUnits[arrayDist[attackingUnit].playerIndex].y, currentPlayer);
        //cout << "Et on y est arrive!" << endl;
    }
    else if (nbUnitCanAttack == 0){ //Si aucune unit ne peut se d�placer � port�e d'attaque, on d�place au max;
        it = arrayDist[unitShortestPath].shorterPath.begin();
        cout <<"D�placement sans arriver � port�e avec unit "<<unitShortestPath<< " de type " << allUnits[unitShortestPath].type<< endl;
        cout << "position unit de l'IA s�lectionn�e = "<< allUnits[unitShortestPath].x << " ; " << allUnits[unitShortestPath].y << endl;
        advance(it,arrayDist[unitShortestPath].shorterPath.size());
        //it =arrayDist[unitShortestPath].shorterPath.end();
        cout <<"target location : " << (*it).x << " ; " << (*it).y << endl;
        currentUnit = unitShortestPath;
        action(&allUnits[currentUnit], (*it).x, (*it).y, currentPlayer);
    }

    else if (nbUnitCanAttack >0){ //Sinon on d�place celle qui a la puissance la plus �lev�e, � port�e d'attaque
        it = arrayDist[movingToAttack].shorterPath.begin();
        cout << "IndexAi = " << movingToAttack << " Index converted = " << movingToAttack << endl;
        cout << "D�placement de l'unit� la plus puissante � port�e d'attaque, unit " << movingToAttack << " de type " << allUnits[movingToAttack].type << endl;
        cout << " Type sans utiliser convert index" << allUnits[movingToAttack].type << endl;
        cout << "position unit de l'IA s�lectionn�e = "<< allUnits[movingToAttack].x << " ; " << allUnits[movingToAttack].y << endl;
        advance(it,arrayDist[movingToAttack].shorterPath.size());
        //it = arrayDist[movingToAttack].shorterPath.end();
        currentUnit = movingToAttack;
        cout <<"target location : " << (*it).x << " ; " << (*it).y << endl;
        action(&allUnits[currentUnit], (*it).x, (*it).y, currentPlayer);
    }
    cout << "Une action a �t� faite" << endl;
}

int convertIndex(int indexAI){
    for (int i = 0; i< unitCount ; i++){
        if (allUnits_IA[indexAI].x == allUnits[i].x && allUnits_IA[indexAI].y == allUnits[i].y){
            return i;
        }
    }
}
