#include "astar.h"
#include <iostream>
using namespace std;

l_noeud liste_ouverte;
l_noeud liste_fermee;
list<point> chemin;

struct point arrivee;
noeud depart;


point obstacle1 = {2,0};
point obstacle2 = {2,1};
point obstacle3 = {2,2};
point obstacle4 = {2,3};
point obstacle5 = {2,4};

point obstacle[] = {obstacle1, obstacle2, obstacle3, obstacle4, obstacle5};


int aStar(int depX, int depY, int destX, int destY){
    arrivee.x = destX;
    arrivee.y = destY;

    depart.parent.first  = depX;
    depart.parent.second = depY;

    pair <int,int> courant;

    courant.first  = depX;
    courant.second = depY;
    // ajout de courant dans la liste ouverte

    liste_ouverte[courant]=depart;

    if (true){
            //cout << "parent first avant while astar " << liste_ouverte[courant].parent.first << " ; " << liste_ouverte[courant].parent.second << endl;
    }

    ajouter_liste_fermee(courant);
    ajouter_cases_adjacentes(courant);

    if (liste_ouverte.empty()){
        cout << "Pb liste_ouverte vide" << endl;
    }

    while( !((courant.first == arrivee.x) && (courant.second == arrivee.y))
            &&
           (!liste_ouverte.empty())
         ){

        courant = meilleur_noeud(liste_ouverte);
        ajouter_liste_fermee(courant);
        ajouter_cases_adjacentes(courant);
    }

    if ((courant.first == arrivee.x) && (courant.second == arrivee.y)){
        retrouver_chemin();

    }else{
         //cout << "Erreur, pas de chemin !" << endl;
    }
	return 0;
}

bool checkCell(int x, int y){
    for (int j =0; j < unitCount ; j++){
        if (allUnits[j].x == x && allUnits[j].y == y){
            return true;
        }
    }
    return false;
}

float distance(int x1, int y1, int x2, int y2){
    return sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}

bool deja_present_dans_liste(pair<int,int> n, l_noeud& l){
    l_noeud::iterator i = l.find(n);
    if (i==l.end())
        return false;
    else
        return true;
}

void ajouter_cases_adjacentes(pair <int,int>& n){
    noeud tmp;
    for (int i=n.first-1; i<=n.first+1; i++){
        if ((i<0) || (i>=15))
            continue;
        for (int j=n.second-1; j<=n.second+1; j++){
            if ((j<0) || (j>=15))
                continue;
            if ((i==n.first) && (j==n.second))
                continue;

            if (checkCell(i,j))
                continue;

            pair<int,int> it(i,j);
            if (!deja_present_dans_liste(it, liste_fermee)){
                tmp.cout_g = liste_fermee[n].cout_g + distance(i,j,n.first,n.second);
                tmp.cout_h = distance(i,j,arrivee.x,arrivee.y);
                tmp.cout_f = tmp.cout_g + tmp.cout_h;
                tmp.parent = n;

                if (deja_present_dans_liste(it, liste_ouverte)){
                    if (tmp.cout_f < liste_ouverte[it].cout_f){
                        // Maj si meilleur chemin trouvé

                        liste_ouverte[it]=tmp;
                    }
                }
                else{
                    liste_ouverte[pair<int,int>(i,j)]=tmp;
                }
            }
        }
    }
}

pair<int,int> meilleur_noeud(l_noeud& l){
    float m_coutf = l.begin()->second.cout_f;
    pair<int,int> m_noeud = l.begin()->first;

    for (l_noeud::iterator i = l.begin(); i!=l.end(); i++)
        if (i->second.cout_f< m_coutf){
            m_coutf = i->second.cout_f;
            m_noeud = i->first;
        }

    return m_noeud;
}

void ajouter_liste_fermee(pair<int,int>& p){
    noeud& n = liste_ouverte[p];
    liste_fermee[p]=n;

    if (liste_ouverte.erase(p)==0)
        cerr << "Erreur, le noeud n'apparaît pas dans la liste ouverte, impossible à supprimer" << endl;
    return;
}

void retrouver_chemin(){
    noeud& tmp = liste_fermee[std::pair<int, int>(arrivee.x,arrivee.y)];

    struct point n;
    pair<int,int> prec;
    n.x = arrivee.x;
    n.y = arrivee.y;
    prec.first  = tmp.parent.first;
    prec.second = tmp.parent.second;
    chemin.push_front(n);

    while (prec != pair<int,int>(depart.parent.first,depart.parent.second)){
        n.x = prec.first;
        n.y = prec.second;
        chemin.push_front(n);
        tmp = liste_fermee[tmp.parent];
        prec.first  = tmp.parent.first;
        prec.second = tmp.parent.second;
    }
}

void pathFinder(point dep, point arrivee){
	chemin.clear();
    liste_ouverte.clear();
    liste_fermee.clear();
    //cout <<"FINDING A PATH BABE" << endl;
	aStar(dep.x, dep.y, arrivee.x, arrivee.y);

    list<point> :: iterator i = chemin.begin();
	return ;
}

