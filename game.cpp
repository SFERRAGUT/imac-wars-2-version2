#include "game.h"
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_image.h"
#include <iostream>
#include <list>
using namespace std;

extern TTF_Font *font;
extern SDL_Surface *screen;
extern SDL_Rect zoneMap, zoneVersus, zoneAi, zoneStart, zoneShop, zoneExitShop, zoneBuy1, zoneBuy2, zoneBuy3, zoneHelp, zoneReStart;
extern list<point> chemin;
extern int currentAchat;

bool isRunning = false;
bool isInMenu = true;
bool versusMode = false;
bool aiMode = false;
bool enableStart = false;
bool isInShop = false;
bool inGame = false;
bool selectDest = false;
bool initIA = false;
bool unitBought = false;
bool isInHelp = false;
bool endGame = false;

bool endFirstRound = false;

int selectX=0;
int selectY=0;
int destX=0;
int destY=0;

int currentUnit = -1;
int currentPlayer = 0;

//pour gérer l'affiche dans le display et les actions menées dans la partie
int currentAttaquante = -1;
int currentVictime = -1 ;
bool action_move = false;
bool action_attaque = false;
bool action_tresor = false ;
bool action_buy = false;
bool action_tuer = false;

int money[2] = {10000,10000};

int downedUnit[2] = {0,0};

int unitCount = 0;
int unitCount_IA = 0;
Unit* allUnits;
Unit* allUnits_IA;



void gameClean(){
    cout << "Game quitting..." << endl;
    TTF_CloseFont(font);
    TTF_Quit();
    IMG_Quit();
    SDL_FreeSurface(screen);
    SDL_Quit();
    cout << "Game quit" << endl;
}

void changePlayer(Unit* unit, Unit* allUnits) //passe au joueur suivant (JOUEUR 1 = 0 ; JOUEUR 2 = 1 ; AI = 3)
{
	if (versusMode) { // si nous sommes dans une partie entre humains
        if (currentPlayer == 0){ //si tour de joueur 1
            cout << "Je change de joueur (de 0 a 1)"<< endl;
			currentPlayer = 1; //on passe au joueur 2
		}
        else{
            cout << "Je change de joueur (de 1 a 0)" <<endl;
            currentPlayer = 0;
        }
	}
	else if (aiMode) {// si nous sommes contre un ordi
		if (currentPlayer == 0) {//Si l'humain a joué lancer l'"IA"
			currentPlayer = 1;
			cout << "Je change de joueur (de 0 a IA)" << endl;
			if (!endFirstRound){
				init_IA(currentPlayer);
				endFirstRound = true;
			}
			else{
				game_IA(currentPlayer);//on lance la fonction de l'AI

			}
		}
        else{
            cout << "Je change de joueur (de IA a 0)" << endl;
            currentPlayer = 0;
        }
	}
	unitBought = false;
	return ;
}


bool isOnButton(SDL_Event e, SDL_Rect rect){
    if(e.button.x>rect.x && e.button.x < rect.x + rect.w && e.button.y >rect.y && e.button.y < rect.y + rect.h){
        return true;
    }
    else {
        return false;
    }
}

void action(Unit *unit, int destX, int destY, int currentPlayer){
    bool vide = true;
    action_move = false;
    action_attaque = false;
    action_tresor = false ;
    action_tuer = false;
    if (aiMode && currentPlayer != 1){ //Comme l'IA achète puis bouge, l'action aurait annulée la valeur true de action_buy
        action_buy = false;
        currentAchat = -1;
    }

    for(int i = 0; i < unitCount; i++){
        if(allUnits[i].x == destX && allUnits[i].y == destY){
            // Si la case de destination est occupée par le currentPlayer : la case n'est pas vide, on augmente currentUnit
            if (allUnits[i].owner == currentPlayer){
        		currentUnit = i;
        		vide = false;
        	}
            // Si la case est dans la portée : on attaque
        	else if ((abs(allUnits[currentUnit].x - allUnits[i].x) + abs(allUnits[currentUnit].y - allUnits[i].y)) <= allUnits[currentUnit].range) {
                allUnits[i].pv -= unit->power*unit->pv;
                unit->pv -= 0.2*(allUnits[i].pv*allUnits[i].power);
                vide = false;
	            if (aiMode && currentPlayer ==1){
                    action_attaque = true;
                    currentVictime = allUnits[i].type;
                    currentAttaquante = unit->type;
                }
	            if(unit->pv <= 1){
                    unit->pv = 1;
	            }
	            cout << "L'attaquante : " << unit->type << "Pv de l'attaquant : " << unit->pv << endl;
	            cout << "La victime : " << allUnits[i].type << "Pv de la victime : " << allUnits[i].pv << endl;

                if(allUnits[i].pv <= 0.5){
	                cout << "Bravo, l'unite a ete detruite !" << endl;
	                if(allUnits[i].type <3){
                        if (aiMode && currentPlayer == 1){
                            action_attaque = false;
                            action_tuer = true;
                        }
                        unitCount ++;
                        allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
                        Unit tresor = unit_init(5,-1);
                        tresor.x = destX;
                        tresor.y = destY;
                        allUnits[unitCount-1] = tresor;
                        downedUnit[currentPlayer]++;
                        }

	                else if(allUnits[i].type == 5){
                        money[currentPlayer] += allUnits[i].cost;
                        if (aiMode && currentPlayer ==1){
                            action_attaque = false;
                            action_tresor = true;
                        }
	                }

	                for(int j =i; j<unitCount; j++){
	                    allUnits[j] = allUnits[j+1];
	                }
	                unitCount --;
	            }
	            selectDest = false;
	            currentUnit = -1;
	            changePlayer(&allUnits[currentUnit], allUnits);
        	}
        }
    }
    if(vide){
		cout << "Chemin suivi : ";
    		for (list<point> :: iterator k = chemin.begin(); k != chemin.end() ; k++){
    			cout << "("<<(*k).x <<" ; "<<(*k).y<<")";
    	}

    	list<point> :: iterator it = chemin.begin();
    	cout << "Begin : (" << (*it).x<<" ; "<<(*it).y << ")"<<endl;

    	if (chemin.size() >0){ // Pour vérifier qu'on ne clique pas sur une unitée hors de portée, sinon téléporte en 0,0...
    		cout << "Current unit = " << currentUnit << " ; Mouvement currentUnit : " << allUnits[currentUnit].move << endl;
    		if (chemin.size() > allUnits[currentUnit].move){
    			advance(it,allUnits[currentUnit].move);
    			cout << "après advance : " << (*it).x<<" ; "<<(*it).y << ")"<<endl;
    			cout << "size (dans advance)  " << chemin.size() << endl;
    			if (aiMode && currentPlayer == 1){
                    action_move = true;
                    currentAttaquante = unit->type;
                }
    		}
    		else {
    			advance(it, chemin.size());
    			it--;
    			cout << "après end : " << (*it).x<<" ; "<<(*it).y << ")"<<endl;
    			cout << "size (dans end)  " << chemin.size() << endl;
    			if (aiMode && currentPlayer == 1){
                    action_move = true;
                    currentAttaquante = unit->type;
                }
    		}

    		cout << endl;
	        cout << "On deplace l'unite en (" << (*it).x << ";" << (*it).y << ")." << endl;
	        cout << "Type unit dans action" << unit->type << endl;
	        unit->x = (*it).x;
	        unit->y = (*it).y;

	        selectDest = false;
	        currentUnit = -1;
	        changePlayer(&allUnits[currentUnit], allUnits);
    	}
    }
}

void gameHandleEvent(){
    SDL_Event event;
    SDL_PollEvent(&event);
    //int type = 0; // de la même manière : Tank = 0 ; Soldat = 2; Bazooka = 4
    if(!endFirstRound){
        int unitP1 = 0;
        int unitP2 = 0;
        for(int i=0; i<unitCount; i++){
            if(allUnits[i].owner == 0){
                unitP1 ++;
            }
            else if(allUnits[i].owner == 1){
                unitP2 ++;
            }
        }
        if(unitP1 == 1 && unitP2 == 1){
            endFirstRound = true;
        }
    }

    switch (event.type){
        case SDL_QUIT :
            isRunning = false;
            gameClean();
            break;

        case SDL_MOUSEBUTTONDOWN:
            cout << "Clique en "<< event.button.x << " ; " << event.button.y << endl;
            if (isInMenu){
            	if (isOnButton(event,zoneVersus) && versusMode == false){
            		versusMode = true;
            		aiMode = false;
            	}
            	else if (isOnButton(event,zoneVersus) && versusMode == true){
            		versusMode = false;
            	}
            	else if (isOnButton(event,zoneAi) && aiMode == false){
            		aiMode = true;
            		versusMode = false;

            	}
            	else if (isOnButton(event,zoneAi) && aiMode == true){
            		aiMode = false;
            	}
            	if (versusMode || aiMode){
            		enableStart = true;
            	}
            	else {
            		enableStart = false;
            	}
            	if (isOnButton(event, zoneStart) && enableStart == true){
            		isInMenu = false;
            		inGame =  true;
            	}
            }
            if (endGame){
                if (isOnButton(event,zoneReStart)){
                    restartGame();
            	}
            }
            if(inGame){

                if (versusMode || (aiMode && currentPlayer != 1)){
                    // On vérifie que le joueur clique bien sur la map
                    if((zoneMap.x + zoneMap.w > event.button.x ) && (event.button.x > zoneMap.x)&&(zoneMap.y + zoneMap.h>event.button.y)&&(event.button.y>zoneMap.y)){
                        if (!selectDest){
                            cout << "Joueur qui joue... " << currentPlayer << endl;
                            // Il faudra penser à vérifier qu'il y a bien une unité à l'emplacement cliqué (et qu'elle appartient au joueur
                            selectX = event.button.x - (SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2;
                            selectX = selectX/LARGEUR_TILE;
                            selectX = selectX*(MAP_WIDTH/MAP_HEIGHT);

                            selectY = event.button.y;
                            selectY = selectY/HAUTEUR_TILE;
                            selectY = selectY*(MAP_WIDTH/MAP_HEIGHT);

                            cout << " Coordonnees de la case selectionnee " << endl;
                            cout << "selectX : " << selectX << endl;
                            cout << "selectY : " << selectY << endl;
                            for(int i = 0; i < unitCount; i++){
                                if(allUnits[i].x == selectX && allUnits[i].y == selectY && allUnits[i].owner == currentPlayer){
                                    currentUnit = i;
                                    selectDest = true;
                                    //rangeIndicator(allUnits[currentUnit]);
                                }
                            }
                        }
                        else{
                            destX = event.button.x - (SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2;
                            destX = destX/LARGEUR_TILE;
                            destX = destX*(MAP_WIDTH/MAP_HEIGHT);

                            destY = event.button.y;
                            destY = destY/HAUTEUR_TILE;
                            destY = destY*(MAP_WIDTH/MAP_HEIGHT);

                            cout << " Coordonnees de la destination " << endl;
                            cout << "destX : " << destX << endl;
                            cout << "destY : " << destY << endl;

                            //Il faudra vérifier si l'endroit cliqué est vide dans ce cas on déplace l'unité, dans l'autre on attaque
                            // créer une fonction action(selectX, selectY, destX, destY
                            cout << "dans game, current Unit = " << currentUnit << endl;
                            action(&allUnits[currentUnit], destX, destY, currentPlayer);
                        // Une fois que l'action a été faite pour le currentPlayer (sauf IA), on change de joueur

                            break;
                        }
                    }
                }
            }
            if (isOnButton(event,zoneShop) && isInShop == false && unitBought == false){
                if (versusMode || (aiMode && currentPlayer != 2)){ //empêche le joueur de cliquer sur le shop pendant que l'IA joue
                cout << "On est dans le shop " << endl;
                isInShop = true;
                inGame = false;
                }
                break;
            }
            else if (isInShop == true && (isOnButton(event, zoneShop) || isOnButton(event, zoneExitShop))){
                cout << "On quitte le shop "<< endl;
                isInShop = false;
                inGame = true;
                break;
            }
            else if (isInShop == true){

            	if (isOnButton(event, zoneBuy1) && money[currentPlayer] >= 1000){
            		unitCount ++;
            		allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
            		Unit unit = unit_init(0,currentPlayer);
            		int PosX = unit.x;
            		int PosY = unit.y;
                    Position_unit(&unit, PosX, PosY);
                	cout << "Soldat acheté, il te reste : "<< money[currentPlayer] << "$."<< endl;
                	allUnits[unitCount-1] = unit;
                	unitBought = true;
            	}
            	if (isOnButton(event, zoneBuy2) && money[currentPlayer] >= 2000){
            		unitCount ++;
            		allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
            		Unit unit = unit_init(1,currentPlayer);
                    int PosX = unit.x;
            		int PosY = unit.y;
                    Position_unit(&unit, PosX, PosY);
                	cout << "Bazooka acheté, il te reste : "<< money[currentPlayer]<< "$."<< endl;
                	allUnits[unitCount-1] = unit;
                	unitBought = true;
            	}
            	if (isOnButton(event, zoneBuy3) && money[currentPlayer] >= 3000){
            		unitCount ++;
            		allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
                	Unit unit = unit_init(2,currentPlayer);
                	int PosX = unit.x;
            		int PosY = unit.y;
                    Position_unit(&unit, PosX, PosY);
                	cout << "Tank acheté, il te reste : "<< money[currentPlayer] << "$."<< endl;
                	allUnits[unitCount-1] = unit;
                	unitBought = true;
            	}

            	if (!endFirstRound){
            		int nbUnitPlayer = 0;
            		for (int i = 0 ; i < unitCount ; i++){
            			if (allUnits[i].owner == currentPlayer){
            				nbUnitPlayer ++;
            			}
            		}
            		if (nbUnitPlayer < 3){
            			unitBought = false;
            		}
            	}
            	isInShop = false;
                inGame = true;
                break;
            }
            if (!isInHelp){
            	if (isOnButton(event, zoneHelp)){
            		isInHelp = true;
            		break;
            	}
            }
            else{
            	if (isOnButton(event, zoneHelp)){
            		isInHelp = false;
            		break;
            	}
            }
    }
}
