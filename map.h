#pragma once
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include "SDL/SDL_image.h"

const int MAP_WIDTH = 15;
const int MAP_HEIGHT = 15;

#define LARGEUR_TILE 32
#define HAUTEUR_TILE 32

typedef struct Map {
    int height ;
    int width ;
    int cells[MAP_HEIGHT][MAP_WIDTH];
} Map;

void map_init(Map *map);

void Afficher_map(SDL_Surface* screen,SDL_Surface* tileset,Map *map,int MAP_WIDTH,int MAP_HEIGHT);

