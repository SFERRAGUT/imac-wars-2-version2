#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "display.h"
#include "map.h"
#include "game.h"
#include <list>
#include "SDL/SDL_image.h"
#include <iostream>
#include <list>
using namespace std;
#include<stdio.h>
#include<stdlib.h>



Uint32 frameStart;
int frameTime;
extern bool isRunning, isInMenu, versusMode, aiMode, enableStart, isInShop, endFirstRound, unitBought, isInHelp;
extern bool action_attaque, action_buy, action_move, action_tresor, action_tuer;
extern int unitCount, currentPlayer, currentUnit, currentAttaquante, currentVictime, currentAchat;
extern Unit* allUnits;
extern int money[2];
extern int downedUnit[2];
extern bool inGame, endGame;
extern list<point> chemin;
extern const int costs[3];


char moneyChar1[32];
char moneyChar2[32];

char unitCountChar1[32];
char unitCountChar2[32];


TTF_Font *font, *fontTitres;

SDL_Color blanc = {255,255,255,255};
SDL_Color noir = {0,0,0,255};
SDL_Color vert = {0,255,0,255};

SDL_Surface* screen,*tileset, *tilesetunitJ1, *tilesetunitJ2, *tilesetObstacle, *tilesetTresor , *interface, *mapSurface, *mapUnit, *fond, *gameOver1, *gameOver2, *screenTemp, *gameBg, *shopButton, *shopButtonOff, *shop, *unitShopJ1, *unitShopJ2, *exitButton;
SDL_Surface* buyButton, *buyButtonOff, *indicSurface, *indicSurfaceBis, *indicSurfaceRange, *tour1Surface, *tour2Surface, *costUnitShop, *helpSprite, *helpButton, *reStartButton;
SDL_Surface* moneyText1, *moneyText2, *downedUnit1, *downedUnit2, *unitInfo1TextHP, *unitInfo1TextType, *unitInfo2TextHP, *unitInfo2TextType, *menuBg, *startButtonOff, *startButton, *versusButtonOff, *versusButton, *aiButton, *aiButtonOff;

SDL_Rect screenRect = {0,0,SCREEN_WIDTH,SCREEN_HEIGHT};
SDL_Rect zoneJ1 = {0,0,(SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2,SCREEN_HEIGHT};
SDL_Rect zoneJ2 = {(SCREEN_WIDTH+MAP_WIDTH*LARGEUR_TILE)/2,0,(SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2,SCREEN_HEIGHT};
SDL_Rect zoneStart, zoneLogo, zoneVersus, zoneAi, zoneShop, zoneUnitShop, zoneExitShop, zoneBuy1, zoneBuy2, zoneBuy3, zoneTour1, zoneTour2, zoneCosts, zoneHelp, zoneReStart ;
SDL_Rect shopUnit0Rect = {margeleft,MAP_HEIGHT*HAUTEUR_TILE/2,LARGEUR_TILE,HAUTEUR_TILE};
SDL_Rect shopUnit1Rect = {(MAP_WIDTH*LARGEUR_TILE-LARGEUR_TILE)/2,MAP_HEIGHT*HAUTEUR_TILE/2,LARGEUR_TILE,HAUTEUR_TILE};
SDL_Rect shopUnit2Rect = {margeright,MAP_HEIGHT*HAUTEUR_TILE/2,LARGEUR_TILE,HAUTEUR_TILE};

SDL_Rect rectDestMoney1 = {margeleft, margetop, 1, 1};
SDL_Rect rectDestMoney2 = {margeright, margetop, 1, 1};
SDL_Rect rectDestUnit1 = {margeleft, 2*margetop, 1, 1};
SDL_Rect rectDestUnit2 = {margeright, 2*margetop, 1, 1};
SDL_Rect rectUnitInfo1 = {margeleft, 0, 1, 1};
SDL_Rect rectUnitInfo2 = {margeright, 0, 1, 1};
SDL_Rect zoneMap = {(SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2,0,MAP_WIDTH*LARGEUR_TILE,MAP_HEIGHT*HAUTEUR_TILE};
SDL_Rect unitRect ={0,0,LARGEUR_TILE,HAUTEUR_TILE};
SDL_Rect destShopUnit = {5*LARGEUR_TILE,0,LARGEUR_TILE,HAUTEUR_TILE};


Map mapTerrain;

void display_init(){
	if(SDL_Init(SDL_INIT_EVERYTHING) == 0){
		TTF_Init();
		IMG_Init(IMG_INIT_PNG);

		fond = SDL_CreateRGBSurface(0,SCREEN_WIDTH,SCREEN_HEIGHT,32,0,0,0,255);
		mapSurface = SDL_CreateRGBSurface(0,MAP_WIDTH*LARGEUR_TILE,MAP_HEIGHT*HAUTEUR_TILE,32,0,0,0,0);
		mapUnit = SDL_CreateRGBSurface(0,MAP_WIDTH*LARGEUR_TILE,MAP_HEIGHT*HAUTEUR_TILE,32,0,0,0,0);
		screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_RESIZABLE | SDL_DOUBLEBUF | SDL_HWSURFACE);
		screenTemp = SDL_CreateRGBSurface(0,SCREEN_WIDTH,SCREEN_HEIGHT,32,0,0,0,0);
		unitShopJ1 = SDL_CreateRGBSurface(0,11*LARGEUR_TILE,HAUTEUR_TILE,32,0,0,0,0);
		unitShopJ2 = SDL_CreateRGBSurface(0,11*LARGEUR_TILE,HAUTEUR_TILE,32,0,0,0,0);
		SDL_FillRect(unitShopJ1,NULL,SDL_MapRGB(unitShopJ1->format,0,0,0));
		SDL_FillRect(unitShopJ2,NULL,SDL_MapRGB(unitShopJ2->format,0,0,0));
		SDL_SetColorKey(unitShopJ1, SDL_SRCCOLORKEY, SDL_MapRGB(unitShopJ1->format,0,0,0));
		SDL_SetColorKey(unitShopJ2, SDL_SRCCOLORKEY, SDL_MapRGB(unitShopJ2->format,0,0,0));

		indicSurfaceRange = SDL_CreateRGBSurface(0,LARGEUR_TILE,HAUTEUR_TILE, 32,0xff, 0xff00, 0xff0000, 0xff000000);
		indicSurfaceBis = SDL_CreateRGBSurface(0,LARGEUR_TILE,HAUTEUR_TILE, 32,0xff, 0xff00, 0xff0000, 0xff000000);
		indicSurface = SDL_CreateRGBSurface(0,LARGEUR_TILE,HAUTEUR_TILE, 32,0xff, 0xff00, 0xff0000, 0xff000000);
		SDL_FillRect(indicSurface,NULL,SDL_MapRGB(indicSurface->format,0,0,255));
		SDL_FillRect(indicSurfaceBis,NULL,SDL_MapRGBA(indicSurfaceBis->format,0,0,255,100));
		SDL_FillRect(indicSurfaceRange,NULL,SDL_MapRGBA(indicSurfaceRange->format,255,0,0,100));

		gameBg = IMG_Load("asset/gameBg.png");
		menuBg = IMG_Load("asset/menuBg.png");
		gameOver1 = IMG_Load("asset/gameover1.png");
		gameOver2 = IMG_Load("asset/gameover2.png");
		tour1Surface = IMG_Load("asset/tourJ1.png");
		tour2Surface = IMG_Load("asset/tourJ2.png");
		helpSprite = IMG_Load("asset/helpSprite.png");
		helpButton = IMG_Load("asset/helpButton.png");

		tileset = IMG_Load("asset/RPGpack_sheet.png");
		tilesetunitJ1 = IMG_Load("asset/unit_j1.png");
		tilesetunitJ2 = IMG_Load("asset/unit_j2.png");

        startButton = IMG_Load("asset/startButton.png");
        reStartButton = IMG_Load("asset/restartButton.png");

		startButtonOff = IMG_Load("asset/startButtonOff.png");
		versusButton = IMG_Load("asset/versusButton.png");
		versusButtonOff = IMG_Load("asset/versusButtonOff.png");
		aiButton = IMG_Load("asset/aiButton.png");
		aiButtonOff = IMG_Load("asset/aiButtonOff.png");
		shopButton = IMG_Load("asset/shopButton.png");
		shopButtonOff = IMG_Load("asset/shopButtonOff.png");
		shop = IMG_Load("asset/shop.png");
		exitButton = IMG_Load("asset/exitButton.png");
		buyButton = IMG_Load("asset/buyButton.png");
		buyButtonOff = IMG_Load("asset/buyButtonOff.png");
		tilesetunitJ1 = IMG_Load("asset/unit_j1.png");
		tilesetunitJ2 = IMG_Load("asset/unit_j2.png");
		tilesetObstacle = IMG_Load("asset/obstacles.png");
		tilesetTresor = IMG_Load("asset/treasure_chest.png");

		SDL_WM_SetCaption("IMACWARS 2", NULL);
		font = TTF_OpenFontIndex("asset/YoureGone.ttf",SCREEN_HEIGHT/40,0);
		fontTitres = TTF_OpenFontIndex("asset/YoureGone.ttf",SCREEN_HEIGHT/20,0);
		TTF_GetError();
		char unitCosts[150];
		sprintf(unitCosts,"$%i                                        $%i                                      $%i",costs[0],costs[1],costs[2]);
		costUnitShop = TTF_RenderUTF8_Blended(font,unitCosts,blanc);
		unitRect.x = 0;
		SDL_BlitSurface(tilesetunitJ1, &unitRect, unitShopJ1, NULL);
		SDL_BlitSurface(tilesetunitJ2, &unitRect, unitShopJ2, NULL);
		unitRect.x = 32;
		destShopUnit.x = 5*LARGEUR_TILE;
		SDL_BlitSurface(tilesetunitJ1, &unitRect, unitShopJ1, &destShopUnit );
		SDL_BlitSurface(tilesetunitJ2, &unitRect, unitShopJ2, &destShopUnit );
		unitRect.x = 64;
		destShopUnit.x = 10*LARGEUR_TILE;
		SDL_BlitSurface(tilesetunitJ1, &unitRect, unitShopJ1, &destShopUnit);
		SDL_BlitSurface(tilesetunitJ2, &unitRect, unitShopJ2, &destShopUnit);

		zoneStart.x = (SCREEN_WIDTH - startButton->w)/2;
		zoneStart.y = 2*SCREEN_HEIGHT/3 +startButton->h*3/4;
		zoneStart.w = startButton->w;
		zoneStart.h = startButton->h;

		zoneReStart.x = (SCREEN_WIDTH - reStartButton->w)/2;
		zoneReStart.y = SCREEN_HEIGHT/2 +reStartButton->h*3/4;
		zoneReStart.w = reStartButton->w;
		zoneReStart.h = reStartButton->h;

		zoneVersus.x = (SCREEN_WIDTH/3 - versusButton->w/2) - 10;
		zoneVersus.y = 2*SCREEN_HEIGHT/3 - 30;
		zoneVersus.w = versusButton->w;
		zoneVersus.h = versusButton->h;

		zoneAi.x = (2*SCREEN_WIDTH/3 - versusButton->w/2 + 10);
		zoneAi.y = 2*SCREEN_HEIGHT/3 - 30;
		zoneAi.w = versusButton->w;
		zoneAi.h = versusButton->h;

		zoneTour1.x = zoneMap.x + 10;
		zoneTour1.y = zoneMap.h + 10;
		zoneTour1.w = tour1Surface->w;
		zoneTour1.h = tour2Surface->h;

		zoneTour2.x = zoneMap.x + zoneMap.w - tour2Surface->w - 10;
		zoneTour2.y = zoneTour1.y;
		zoneTour2.w = tour2Surface->w;
		zoneTour2.h = tour2Surface->h;

		zoneShop.x = (SCREEN_WIDTH - shopButton->w ) /2;
		zoneShop.y = (SCREEN_HEIGHT - MAP_HEIGHT*LARGEUR_TILE)/ 2 + MAP_HEIGHT*LARGEUR_TILE - shopButton->h/2 - 20;
		zoneShop.w = shopButton->w;
		zoneShop.h = shopButton->h;

		zoneUnitShop.x = (shop->w - unitShopJ1->w )/2;
		zoneUnitShop.y = (shop->h - unitShopJ1->h)/2 - 2*HAUTEUR_TILE;
		zoneUnitShop.w = unitShopJ1->w;
		zoneUnitShop.h = unitShopJ1->h;

		zoneExitShop.x = (shop->w - exitButton->w )/2;
		zoneExitShop.y = shop->h - 4*exitButton->h;
		zoneExitShop.w = exitButton->w;
		zoneExitShop.h = exitButton->h;

		zoneCosts.x = zoneUnitShop.x - 10;
		zoneCosts.y = zoneUnitShop.y + costUnitShop->h + 20;
		zoneCosts.w = costUnitShop->w;
		zoneCosts.h = costUnitShop->h;


		zoneBuy1.h = buyButton->h;
		zoneBuy1.w = buyButton->w;
		zoneBuy1.x = zoneMap.x + 2*LARGEUR_TILE - 15;
		zoneBuy1.y = shop->h - 5*exitButton->h;

		zoneBuy2 = zoneBuy1;
		zoneBuy3 = zoneBuy1;

		zoneBuy2.x =  zoneMap.x + 7*LARGEUR_TILE - 15;
		zoneBuy3.x = zoneMap.x + 12*LARGEUR_TILE - 15;

		zoneHelp.x = zoneMap.x + zoneMap.w - helpButton->w - 10;
		zoneHelp.y = zoneShop.y + zoneShop.h - 10;
		zoneHelp.w = helpButton->w;
		zoneHelp.h = helpButton->h;

		SDL_BlitSurface(exitButton,NULL,shop,&zoneExitShop);

		//shopInit();

		if (mapSurface == NULL){
			cout << "Pb surface" << endl;
		}
		if (!startButton){
			cout << "erreur chargement startButton.png" << endl;
		}
		if (!menuBg){
			cout << "Erreur chargement menuBg.png" << endl;
		}
        if (!tileset)
        {
            printf("Echec de chargement tile_map2.png\n");
            SDL_Quit();
            system("pause");
            exit(-1);
        }
        if (!tilesetunitJ1)
        {
            printf("Echec de chargement unit_j1.png\n");
            SDL_Quit();
            system("pause");
            exit(-1);
        }
        if (!tilesetunitJ2)
        {
            printf("Echec de chargement unit_j2.png\n");
            SDL_Quit();
            system("pause");
            exit(-1);
        }

		if (screen){
			cout << "Initialised..!" << endl;
		}


		if (TTF_Init()){
			cout << "TTF Initialised"<< endl;
		}

        map_init(&mapTerrain);

        Afficher_map(mapSurface,tileset,&mapTerrain,MAP_WIDTH,MAP_HEIGHT);
        isRunning = true;
	}

	else{
		cout << "Erreur d'initialisation de la SDL : ";
		cerr << SDL_GetError() << endl;
		SDL_Quit();
	}
}

void display_update(){
	SDL_UpdateRect(screen, 0, 0, 0, 0);
	//SDL_Flip(screen);
	frameStart = SDL_GetTicks();

 //   if (not isInMenu){
	// 	display(&game);
	// }

	// else {
	//  	display_menu();
	// }
	if (isInMenu){
	    display_menu();
	}
	else{
		display();
	}

	frameTime = SDL_GetTicks() - frameStart;
	gameHandleEvent();
	if (frameDelay > frameTime){
		SDL_Delay(frameDelay - frameTime);
	}
}

void display(){
	//blit la map et les unit + l'UI ou les menus,
	//dépend du contenu des autres fichiers
	SDL_BlitSurface(fond,NULL,screenTemp,NULL);
	SDL_BlitSurface(gameBg,NULL,screenTemp,NULL);

	if (unitBought == false){
		SDL_BlitSurface(shopButton,NULL,screenTemp,&zoneShop);
	}
	else if (unitBought == true){
		SDL_BlitSurface(shopButtonOff,NULL,screenTemp,&zoneShop);
	}

	SDL_BlitSurface(helpButton,NULL,screenTemp,&zoneHelp);

    SDL_BlitSurface(mapSurface,NULL,screenTemp,&zoneMap);

    if (currentUnit >=0){
	    rangeIndicator(currentUnit);
	}

    for(int i=0; i<unitCount; i++){
        display_unit(allUnits[i]);
        //cout << "Test affichage unit" << endl;
    }
    display_info();
    display_winner();

    if (isInShop){
    	SDL_BlitSurface(shop,NULL,screenTemp,NULL);
    	// if l'argent le permet, en rouge, sinon en gris, à ajouter
    	if (money[currentPlayer] >= 3000){
    		SDL_BlitSurface(buyButton,NULL,screenTemp, &zoneBuy1);
    		SDL_BlitSurface(buyButton, NULL, screenTemp, &zoneBuy2);
    		SDL_BlitSurface(buyButton, NULL, screenTemp, &zoneBuy3);
    	}
    	else if (money[currentPlayer] >= 2000){
    		SDL_BlitSurface(buyButton,NULL,screenTemp, &zoneBuy1);
    		SDL_BlitSurface(buyButton, NULL, screenTemp, &zoneBuy2);
    		SDL_BlitSurface(buyButtonOff, NULL, screenTemp, &zoneBuy3);
    	}
    	else if (money[currentPlayer] >= 1000){
    		SDL_BlitSurface(buyButton,NULL,screenTemp, &zoneBuy1);
    		SDL_BlitSurface(buyButtonOff, NULL, screenTemp, &zoneBuy2);
    		SDL_BlitSurface(buyButtonOff, NULL, screenTemp, &zoneBuy3);
    	}
    	else if (money[currentPlayer] < 1000){
    		SDL_BlitSurface(buyButtonOff, NULL,screenTemp, &zoneBuy1);
    		SDL_BlitSurface(buyButtonOff, NULL, screenTemp, &zoneBuy2);
    		SDL_BlitSurface(buyButtonOff, NULL, screenTemp, &zoneBuy3);
    	}
    	if (currentPlayer == 0){
    		SDL_BlitSurface(unitShopJ1,NULL,screenTemp,&zoneUnitShop);
    	}
    	else if (currentPlayer == 1){
    		SDL_BlitSurface(unitShopJ2,NULL,screenTemp,&zoneUnitShop);
    	}

    	SDL_BlitSurface(costUnitShop,NULL,screenTemp,&zoneCosts);
    }
    if (isInHelp){
		SDL_BlitSurface(helpSprite,NULL,screenTemp,NULL);
		SDL_BlitSurface(exitButton,NULL,screenTemp,&zoneHelp);
	}
    if (currentPlayer == 0){
    	SDL_BlitSurface(tour1Surface,NULL,screenTemp,&zoneTour1);
    }
    else{
    	SDL_BlitSurface(tour2Surface, NULL,screenTemp,&zoneTour2);
    }

    SDL_BlitSurface(screenTemp,NULL,screen,NULL);
}

void display_menu(){

	SDL_BlitSurface(menuBg,NULL,screenTemp,&screenRect);
	if (enableStart){
		SDL_BlitSurface(startButton,NULL,screenTemp,&zoneStart);
	}
	else {
		SDL_BlitSurface(startButtonOff,NULL,screenTemp,&zoneStart);
	}
	if (versusMode){
		SDL_BlitSurface(versusButton,NULL,screenTemp,&zoneVersus);
		SDL_BlitSurface(aiButtonOff,NULL,screenTemp,&zoneAi);
	}
	else {
		SDL_BlitSurface(versusButtonOff,NULL,screenTemp,&zoneVersus);
	}
	if (aiMode){
		SDL_BlitSurface(aiButton,NULL,screenTemp,&zoneAi);
	}
	else {
		SDL_BlitSurface(aiButtonOff,NULL,screenTemp,&zoneAi);
	}
	SDL_BlitSurface(screenTemp,NULL,screen,NULL);
}

void display_unit(Unit unit){
    SDL_Rect Rect_dest;
	SDL_Rect Rect_source;

	Rect_source.w = LARGEUR_TILE;
    Rect_source.h = HAUTEUR_TILE;

	Rect_dest.x = zoneMap.x + unit.x*LARGEUR_TILE;
    Rect_dest.y = unit.y *HAUTEUR_TILE;

	Rect_source.x = (unit.type)*LARGEUR_TILE;
    Rect_source.y = 0;

    switch(unit.owner){
        case 0:
           SDL_BlitSurface(tilesetunitJ1,&Rect_source,screenTemp,&Rect_dest);
           SDL_Flip(screen);
           break;
        case 1:
            SDL_BlitSurface(tilesetunitJ2,&Rect_source,screenTemp,&Rect_dest);
            SDL_Flip(screen);
            break;
        case -1:
            if(unit.type == 5){
                Rect_source.x = (unit.type-5)*LARGEUR_TILE;
                SDL_BlitSurface(tilesetTresor,&Rect_source,screenTemp,&Rect_dest);
                SDL_Flip(screen);
            }
            else{
                Rect_source.x = (unit.type-10)*LARGEUR_TILE;
                SDL_BlitSurface(tilesetObstacle,&Rect_source,screenTemp,&Rect_dest);
                SDL_Flip(screen);
            }
            break;
        default:
            cout << "Erreur : Cette unité n'a pas de propriétaire !" << endl;
            break;
    }
}

void display_info(){
    sprintf(moneyChar1, "JOUEUR 1 : %d$",money[0]);
    sprintf(moneyChar2, "JOUEUR 2 : %d$",money[1]);
    sprintf(unitCountChar1, "Unités abattues : %d", downedUnit[0]);
    sprintf(unitCountChar2, "Unités abattues : %d", downedUnit[1]);


	rectUnitInfo1.y = 2*margetop + rectDestMoney1.h;
	rectUnitInfo2.y = 2*margetop + rectDestMoney2.h;

    char hp[10];
    char unitType[10];
    char unitInfo1Type[64] = {'\0'};
    char unitInfo1HP[64] = {'\0'};
	char unitInfo2Type[64]= {'\0'};
	char unitInfo2HP[64]= {'\0'};
	rectUnitInfo1.y = 4*margetop + rectDestMoney1.h;
	rectUnitInfo2.y = 4*margetop + rectDestMoney2.h;

	for(int i=0; i<unitCount; i++){
        if(allUnits[i].type == 0){
            sprintf(unitType, " Soldat");
        }
        else if(allUnits[i].type == 1){
            sprintf(unitType, " Bazooka");
        }
        else if(allUnits[i].type == 2){
            sprintf(unitType, " Tank");
        }
       if(allUnits[i].owner==0){

            sprintf(unitInfo1Type,"Type : "); // LDH
            strcat(unitInfo1Type,unitType);
            if (i == currentUnit){
       			strcat(unitInfo1Type, " <");
       			//cout << unitInfo1Type<< endl;
       		}
            sprintf(unitInfo1HP,"HP : ");
            sprintf(hp, "%i",(int)allUnits[i].pv );
            strcat(unitInfo1HP,hp);
            unitInfo1TextType = TTF_RenderUTF8_Blended(font,unitInfo1Type, blanc);
            unitInfo1TextHP = TTF_RenderUTF8_Blended(font,unitInfo1HP, blanc);
            rectUnitInfo1.w = unitInfo1TextType->w;
            rectUnitInfo1.h = unitInfo1TextType->h;
            SDL_BlitSurface(unitInfo1TextType,NULL,screenTemp,&rectUnitInfo1);
            rectUnitInfo1.y += margetop;
            SDL_BlitSurface(unitInfo1TextHP,NULL,screenTemp,&rectUnitInfo1);
            rectUnitInfo1.y += margetop + unitInfo1TextHP->h ;
       }
       else if (allUnits[i].owner==1){

            sprintf(unitInfo2Type,"Type : ");
            strcat(unitInfo2Type,unitType);
            if (i == currentUnit){
       			strcat(unitInfo2Type, " <");
       			//cout << unitInfo2Type<< endl;
       		}
            sprintf(unitInfo2HP,"HP : ");
            sprintf(hp, "%i",(int)allUnits[i].pv);
            strcat(unitInfo2HP,hp);
            unitInfo2TextType = TTF_RenderUTF8_Blended(font,unitInfo2Type, blanc);
            unitInfo2TextHP = TTF_RenderUTF8_Blended(font,unitInfo2HP, blanc);
            rectUnitInfo2.w = unitInfo2TextType->w;
            rectUnitInfo2.h = unitInfo2TextType->h;
            SDL_BlitSurface(unitInfo2TextType,NULL,screenTemp,&rectUnitInfo2);
            rectUnitInfo2.y += margetop;
            SDL_BlitSurface(unitInfo2TextHP,NULL,screenTemp,&rectUnitInfo2);
            rectUnitInfo2.y += margetop + unitInfo2TextHP->h;
       }
    }

    if (aiMode){
        if(endFirstRound){
            SDL_Surface* infoActionSurface;
            char action[80];
            char unitTypeVictime[10];
            char tmpVictime[10];
            char unitTypeAttaquante[10];
            char unitTypeAchat[10];

            //cout << "victime : " << currentVictime <<" // attaquante : "<< currentAttaquante << " // achat :" << currentAchat << endl;
            if(currentVictime == 0){
                sprintf(unitTypeVictime, "Soldat");
            }
            else if(currentVictime == 1){
                sprintf(unitTypeVictime, "Bazooka");
            }
            else if(currentVictime == 2){
                sprintf(unitTypeVictime, "Tank");
            }
            if(currentAttaquante == 0){
                sprintf(unitTypeAttaquante, "Soldat");
            }
            else if(currentAttaquante == 1){
                sprintf(unitTypeAttaquante, "Bazooka");
            }
            else if(currentAttaquante == 2){
                sprintf(unitTypeAttaquante, "Tank");
            }
            if(currentAchat == 0){
                sprintf(unitTypeAchat, "Soldat");
            }
            else if(currentAchat == 1){
                sprintf(unitTypeAchat, "Bazooka");
            }
            else if(currentAchat == 2){
                sprintf(unitTypeAchat, "Tank");
            }
            //cout << "victime : " << unitTypeVictime <<" // attaquante : "<< unitTypeAttaquante << " // achat :" << unitTypeAchat << endl;
            if (action_attaque && !action_tuer && !action_tresor){
                sprintf(action,"L'ennemi a attaqué votre ");
                strcat(action,unitTypeVictime);
                strcat(action," avec son ");
                strcat(action,unitTypeAttaquante);

                infoActionSurface = TTF_RenderUTF8_Blended(font,action, blanc);
                SDL_Rect zoneAction = {(SCREEN_WIDTH - infoActionSurface->w)/2, zoneShop.y + zoneShop.h + 20, infoActionSurface->w, infoActionSurface->h};
                SDL_BlitSurface(infoActionSurface,NULL,screenTemp,&zoneAction);
                SDL_FreeSurface(infoActionSurface);

            }
            else if (action_tresor){
                sprintf(action, "L'ennemi a récupéré un trésor !");
                infoActionSurface = TTF_RenderUTF8_Blended(font,action, blanc);
                SDL_Rect zoneAction = {(SCREEN_WIDTH - infoActionSurface->w)/2, zoneShop.y + zoneShop.h + 20, infoActionSurface->w, infoActionSurface->h};
                SDL_BlitSurface(infoActionSurface,NULL,screenTemp,&zoneAction);
                SDL_FreeSurface(infoActionSurface);
            }

            else if (action_move){
                sprintf(action,"Déplacement de son ");
                strcat(action,unitTypeAttaquante);

                if (action_buy){
                    strcat(action," et achat d'un ");
                    strcat(action, unitTypeAchat);
                }
                infoActionSurface = TTF_RenderUTF8_Blended(font,action, blanc);
                SDL_Rect zoneAction = {(SCREEN_WIDTH - infoActionSurface->w)/2, zoneShop.y + zoneShop.h + 20, infoActionSurface->w, infoActionSurface->h};
                SDL_BlitSurface(infoActionSurface,NULL,screenTemp,&zoneAction);
                SDL_FreeSurface(infoActionSurface);
            }
            else if (action_tuer){
                sprintf(action,"Le ");
                strcat(action,unitTypeAttaquante);
                strcat(action,"a détruit une de vos unités !");
                //strcat(action,unitTypeVictime);
                infoActionSurface = TTF_RenderUTF8_Blended(font,action, blanc);
                SDL_Rect zoneAction = {(SCREEN_WIDTH - infoActionSurface->w)/2, zoneShop.y + zoneShop.h + 20, infoActionSurface->w, infoActionSurface->h};
                SDL_BlitSurface(infoActionSurface,NULL,screenTemp,&zoneAction);
                SDL_FreeSurface(infoActionSurface);
            }
        }
    }




    if (!font){
        cout << "Police non initialisée :" << TTF_GetError()<< endl;
    }

    moneyText1 = TTF_RenderUTF8_Blended(font,moneyChar1, blanc);
    moneyText2 = TTF_RenderUTF8_Blended(font,moneyChar2, blanc);

    downedUnit1 = TTF_RenderUTF8_Blended(font,unitCountChar1, blanc);
    downedUnit2 = TTF_RenderUTF8_Blended(font,unitCountChar2, blanc);

    rectDestMoney1.w = moneyText1->w;
    rectDestMoney1.h = moneyText1->h;
    rectDestMoney2.w = moneyText2->w;
    rectDestMoney2.h = moneyText2->h;

    rectDestUnit1.w = downedUnit1->w;
    rectDestUnit1.h = downedUnit1->h;
    rectDestUnit2.w = downedUnit2->w;
    rectDestUnit2.h = downedUnit2->h;

    SDL_BlitSurface(moneyText1,NULL,screenTemp,&rectDestMoney1);
    SDL_BlitSurface(moneyText2,NULL,screenTemp,&rectDestMoney2);

    SDL_BlitSurface(downedUnit1,NULL,screenTemp,&rectDestUnit1);
    SDL_BlitSurface(downedUnit2,NULL,screenTemp,&rectDestUnit2);

   	SDL_FreeSurface(moneyText1);
   	SDL_FreeSurface(moneyText2);
   	SDL_FreeSurface(downedUnit1);
   	SDL_FreeSurface(downedUnit2);
}

void rangeIndicator(int selectedUnit) {

	SDL_Rect indic = {0,0,LARGEUR_TILE,HAUTEUR_TILE};
	//SDL_Rect rectDest = {0,0,LARGEUR_TILE,HAUTEUR_TILE};

	int nbDeplacement = 0;
	int move = allUnits[selectedUnit].move;
	int mouse_x, mouse_y;
	SDL_GetMouseState(&mouse_x, &mouse_y);

	int X = mouse_x - (SCREEN_WIDTH-MAP_WIDTH*LARGEUR_TILE)/2;
    X = X/LARGEUR_TILE;
    X = X*(MAP_WIDTH/MAP_HEIGHT);

    int Y = mouse_y;
    Y = Y/HAUTEUR_TILE;
    Y = Y*(MAP_WIDTH/MAP_HEIGHT);

    point dep = {allUnits[selectedUnit].x,allUnits[selectedUnit].y};
    point ar = {X,Y};

    //cout << chemin.size() << endl;
    pathFinder(dep,ar);
    //cout << chemin.size() << endl;
    list<point> path = chemin;
    list<point>:: iterator it = path.begin();

    //cout << "(" << X << " ; " << Y << ") ; ("<<dep.x<<" ; "<< dep.y << ")-> Taille = " << path.size()<< endl;
    for (it = path.begin(); it != path.end(); it ++){
    	indic.x = (*it).x * LARGEUR_TILE + (SCREEN_WIDTH - zoneMap.w)/2;
    	indic.y = (*it).y * HAUTEUR_TILE;
    	SDL_BlitSurface(indicSurfaceBis, NULL, screenTemp, &indic);

    	if (nbDeplacement <= move){
    		SDL_BlitSurface(indicSurface, NULL, screenTemp, &indic);
    	}
    	nbDeplacement++;
    }

	SDL_Rect rectDest = {zoneMap.x,0,LARGEUR_TILE,HAUTEUR_TILE};
	int delta;
	for (int i = 0; i < MAP_WIDTH ; i ++ ){
		rectDest.x = i * LARGEUR_TILE + zoneMap.x;
		for (int j = 0; j < MAP_HEIGHT ; j++){
			rectDest.y = j*HAUTEUR_TILE;

			delta = abs(i - allUnits[selectedUnit].x) + abs(j - allUnits[selectedUnit].y);
			if (delta <= allUnits[selectedUnit].range){
				//cout << delta<<endl;
				SDL_BlitSurface(indicSurfaceRange,NULL,screenTemp,&rectDest);
			}
		}
	}

	SDL_Surface* statUnitSurface;
	char pow[16];
	sprintf(pow,"%i",(int)(100*allUnits[currentUnit].power));
	char mov[10];
	sprintf(mov,"%i",allUnits[currentUnit].move+1);
	char ran[10];
	sprintf(ran,"%i",allUnits[currentUnit].range);

	char stats[40] = "MOV : ";
	strcat(stats,mov);
	strcat(stats," | POW : ");
	strcat(stats,pow);
	strcat(stats," | RAN : ");
	strcat(stats,ran);
	statUnitSurface = TTF_RenderUTF8_Blended(font,stats, blanc);

	SDL_Rect zoneStats = {(SCREEN_WIDTH - statUnitSurface->w)/2, zoneTour1.y, statUnitSurface->w, statUnitSurface->h};

	SDL_BlitSurface(statUnitSurface,NULL,screenTemp,&zoneStats);

	SDL_FreeSurface(statUnitSurface);
}

void display_winner(){
    if(endFirstRound){
        int unitP1 = 0;
        int unitP2 = 0;
        for(int i=0; i<unitCount; i++){
            if(allUnits[i].owner == 0){
                unitP1 ++;
            }
            else if(allUnits[i].owner == 1){
                unitP2 ++;
            }
        }
        if(unitP1 == 0){
            endGame = true;
            inGame = false;
            SDL_BlitSurface(gameOver2,NULL,screenTemp,&screenRect);
            SDL_BlitSurface(reStartButton,NULL,screenTemp,&zoneReStart);
        }
        else if(unitP2 == 0){
            endGame = true;
            inGame = false;
            SDL_BlitSurface(gameOver1,NULL,screenTemp,&screenRect);
            SDL_BlitSurface(reStartButton,NULL,screenTemp,&zoneReStart);
        }
    }
}

void restartGame(){
    isInMenu = true;
    versusMode = false;
    aiMode = false;
    enableStart = false;
    endFirstRound = false;
    isRunning = false;
    currentUnit = -1;
    currentPlayer = 0;
    money[0] = 10000;
    money[1] = 10000;
    downedUnit[0] = 0;
    downedUnit[1] = 0;
    unitCount = 0;
    allUnits = NULL;
    SDL_FreeSurface(screenTemp);
    //unitRect.x = 32;
    display_init();
    endGame = false;
}
