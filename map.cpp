#include <iostream>
#include <bits/stdc++.h>
using namespace std;
#include "map.h"
#include "game.h"

extern int unitCount;
extern Unit* allUnits;


void map_init(Map *map) {
    map->width = MAP_WIDTH;
    map->height = MAP_HEIGHT;
    for (int y=0; y<map->height; y++){
        for (int x=0; x<map->width; x++){
            map->cells[y][x] = 6;
        }
    }
    map->cells[0][3]= 210;
    map->cells[0][4]= 212;
    map->cells[0][5]= 213;
    map->cells[0][6]= 254;
    map->cells[0][7]= 257;
    map->cells[0][8]= 170;
    map->cells[0][9]= 96;
    map->cells[0][10]= 137;
    map->cells[0][11]= 175;
    map->cells[1][8]= 210;
    map->cells[1][9]= 213;
    map->cells[1][10]= 213;
    map->cells[1][11]= 215;

    map->cells[3][1]= 47;
    map->cells[3][2]= 48;
    map->cells[4][1]= 87;
    map->cells[4][2]= 88;

    map->cells[7][5]= 20;
    map->cells[7][6]= 21;
    map->cells[7][7]= 25;
    map->cells[8][5]= 60;
    map->cells[8][6]= 61;
    map->cells[8][7]= 65;
    map->cells[9][5]= 140;
    map->cells[9][6]= 101;
    map->cells[9][7]= 145;
    map->cells[10][5]= 180;
    map->cells[10][6]= 141;
    map->cells[10][7]= 185;
    map->cells[11][5]= 220;
    map->cells[11][6]= 221;
    map->cells[11][7]= 225;

    map->cells[13][0]= 24;
    map->cells[13][1]= 25;
    map->cells[14][0]= 62;
    map->cells[14][1]= 65;

    map->cells[11][13]= 20;
    map->cells[11][14]= 21;
    map->cells[12][13]= 60;
    map->cells[12][14]= 66;
    map->cells[13][13]= 100;
    map->cells[13][14]= 66;
    map->cells[14][13]= 180;
    map->cells[14][14]= 66;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit = unit_init(10,-1);
    unit.x = 5;
    unit.y = 5;
    allUnits[unitCount-1] = unit;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit2 = unit_init(12,-1);
    unit2.x = 6;
    unit2.y = 5;
    allUnits[unitCount-1] = unit2;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit37 = unit_init(18,-1);
    unit37.x = 6;
    unit37.y = 4;
    allUnits[unitCount-1] = unit37;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit38 = unit_init(16,-1);
    unit38.x = 4;
    unit38.y = 5;
    allUnits[unitCount-1] = unit38;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit39 = unit_init(17,-1);
    unit39.x = 4;
    unit39.y = 4;
    allUnits[unitCount-1] = unit39;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit3 = unit_init(11,-1);
    unit3.x = 7;
    unit3.y = 5;
    allUnits[unitCount-1] = unit3;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit4 = unit_init(15,-1);
    unit4.x = 9;
    unit4.y = 5;
    allUnits[unitCount-1] = unit4;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit5 = unit_init(14,-1);
    unit5.x = 9;
    unit5.y = 4;
    allUnits[unitCount-1] = unit5;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit40 = unit_init(18,-1);
    unit40.x = 10;
    unit40.y = 0;
    allUnits[unitCount-1] = unit40;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit41 = unit_init(10,-1);
    unit41.x = 9;
    unit41.y = 1;
    allUnits[unitCount-1] = unit41;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit42 = unit_init(11,-1);
    unit42.x = 10;
    unit42.y = 1;
    allUnits[unitCount-1] = unit42;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit43 = unit_init(17,-1);
    unit43.x = 3;
    unit43.y = 12;
    allUnits[unitCount-1] = unit43;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit44 = unit_init(16,-1);
    unit44.x = 5;
    unit44.y = 0;
    allUnits[unitCount-1] = unit44;

    // EAU
    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit6 = unit_init(4,-1);
    unit6.x = 5;
    unit6.y = 7;
    allUnits[unitCount-1] = unit6;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit7 = unit_init(4,-1);
    unit7.x = 6;
    unit7.y = 7;
    allUnits[unitCount-1] = unit7;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit8 = unit_init(4,-1);
    unit8.x = 7;
    unit8.y = 7;
    allUnits[unitCount-1] = unit8;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit9 = unit_init(4,-1);
    unit9.x = 5;
    unit9.y = 8;
    allUnits[unitCount-1] = unit9;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit10 = unit_init(4,-1);
    unit10.x = 6;
    unit10.y = 8;
    allUnits[unitCount-1] = unit10;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit11 = unit_init(4,-1);
    unit11.x = 7;
    unit11.y = 8;
    allUnits[unitCount-1] = unit11;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit12 = unit_init(4,-1);
    unit12.x = 5;
    unit12.y = 9;
    allUnits[unitCount-1] = unit12;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit13 = unit_init(4,-1);
    unit13.x = 6;
    unit13.y = 9;
    allUnits[unitCount-1] = unit13;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit14 = unit_init(4,-1);
    unit14.x = 7;
    unit14.y = 9;
    allUnits[unitCount-1] = unit14;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit15 = unit_init(14,-1);
    unit15.x = 5;
    unit15.y = 10;
    allUnits[unitCount-1] = unit15;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit16 = unit_init(4,-1);
    unit16.x = 6;
    unit16.y = 10;
    allUnits[unitCount-1] = unit16;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit17 = unit_init(4,-1);
    unit17.x = 7;
    unit17.y = 10;
    allUnits[unitCount-1] = unit17;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit18 = unit_init(4,-1);
    unit18.x = 5;
    unit18.y = 11;
    allUnits[unitCount-1] = unit18;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit19 = unit_init(4,-1);
    unit19.x = 6;
    unit19.y = 11;
    allUnits[unitCount-1] = unit19;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit20 = unit_init(4,-1);
    unit20.x = 7;
    unit20.y = 11;
    allUnits[unitCount-1] = unit20;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit21 = unit_init(4,-1);
    unit21.x = 0;
    unit21.y = 13;
    allUnits[unitCount-1] = unit21;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit22 = unit_init(4,-1);
    unit22.x = 1;
    unit22.y = 13;
    allUnits[unitCount-1] = unit22;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit23 = unit_init(4,-1);
    unit23.x = 0;
    unit23.y = 14;
    allUnits[unitCount-1] = unit23;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit24 = unit_init(4,-1);
    unit24.x = 1;
    unit24.y = 14;
    allUnits[unitCount-1] = unit24;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit25 = unit_init(4,-1);
    unit25.x = 13;
    unit25.y = 11;
    allUnits[unitCount-1] = unit25;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit26 = unit_init(4,-1);
    unit26.x = 14;
    unit26.y = 11;
    allUnits[unitCount-1] = unit26;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit27 = unit_init(4,-1);
    unit27.x = 13;
    unit27.y = 12;
    allUnits[unitCount-1] = unit27;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit28 = unit_init(4,-1);
    unit28.x = 14;
    unit28.y = 12;
    allUnits[unitCount-1] = unit28;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit29 = unit_init(4,-1);
    unit29.x = 13;
    unit29.y = 13;
    allUnits[unitCount-1] = unit29;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit30 = unit_init(4,-1);
    unit30.x = 14;
    unit30.y = 13;
    allUnits[unitCount-1] = unit30;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit31 = unit_init(4,-1);
    unit31.x = 13;
    unit31.y = 14;
    allUnits[unitCount-1] = unit31;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit32 = unit_init(4,-1);
    unit32.x = 14;
    unit32.y = 14;
    allUnits[unitCount-1] = unit32;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit33 = unit_init(4,-1);
    unit33.x = 6;
    unit33.y = 0;
    allUnits[unitCount-1] = unit33;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit34 = unit_init(4,-1);
    unit34.x = 7;
    unit34.y = 0;
    allUnits[unitCount-1] = unit34;

    // Autre
    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit35 = unit_init(17,-1);
    unit35.x = 10;
    unit35.y = 8;
    allUnits[unitCount-1] = unit35;

    unitCount ++;
    allUnits = (Unit*) realloc(allUnits, unitCount * sizeof(Unit));
    Unit unit36 = unit_init(13,-1);
    unit36.x = 11;
    unit36.y = 8;
    allUnits[unitCount-1] = unit36;
}

void Afficher_map(SDL_Surface* screen,SDL_Surface* tileset,Map *map,int MAP_WIDTH,int MAP_HEIGHT){
	 int i,j;
	 SDL_Rect Rect_dest;
	 SDL_Rect Rect_source;
	 Rect_source.w = LARGEUR_TILE;
	 Rect_source.h = HAUTEUR_TILE;
	 for(i=0;i<MAP_WIDTH;i++)
	 {
	 	for(j=0;j<MAP_HEIGHT;j++)
	 	{
	 		Rect_dest.x = i*LARGEUR_TILE;
	 		Rect_dest.y = j*HAUTEUR_TILE;
             Rect_source.x = (((int)map->cells[j][i])%40)*LARGEUR_TILE;
             Rect_source.y = ((int)map->cells[j][i]/40)*LARGEUR_TILE;
             SDL_BlitSurface(tileset,&Rect_source,screen,&Rect_dest);
	 	}
	 }
	 SDL_Flip(screen);
}


