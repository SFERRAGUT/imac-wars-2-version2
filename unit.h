#ifndef UNIT_H
#define UNIT_H

const int NB_UNIT = 3;


typedef struct Unit {
    int owner;
    int cost;
    float power;
    int move;
    float pv;
    int range;
    int type;
    int x;
    int y;
} Unit;

extern const char* const UNITS_NAME[NB_UNIT];
extern int moneyJ1, moneyJ2;
extern int money[2];
const int costs[3] = {1000,2000,3000};

//void soldat_init(Unit* soldat);
//
//void bazooka_init(Unit* bazooka);
//
//void tank_init(Unit* tank);

Unit unit_init(int type, int owner);

#endif
