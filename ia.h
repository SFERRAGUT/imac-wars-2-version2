#pragma once
#include "SDL/SDL.h"
#include "unit.h"
#include "game.h"
#include "map.h"
#include "astar.h"
#include <iostream>
#include <map>
#include "math.h"
#include <utility>
#include <list>

using namespace std;

typedef struct distanceCible{
	int aiIndex;
	int playerIndex;
	list<struct point> shorterPath;
	bool canAttack = false;
	bool inRange=false;
}distanceCible;

void init_IA(int currentPlayer);
void game_IA(int currentPlayer);
void buyUnit_IA(int currentPlayer, int type);
void Position_unit(Unit* unit, int PosX, int PosY );
void checkCible(int currentPlayer);

int convertIndex(int indexAI);
