#include "unit.h"

#include <string.h>
#include <stdio.h>

const char* const UNITS_NAME[NB_UNIT] = {"SOLDAT", "BAZOOKA", "TANK"};

Unit unit_init(int type, int owner){
    Unit unit;
    unit.owner = owner;
    unit.x = 0;
    unit.y = 0;

    if(type == 0){
        unit.cost = costs[0];
        unit.power = 0.2;
        unit.move = 6;
        unit.pv = 100;
        unit.range = 1;
        unit.type = 0;
    }
    else if(type == 1){
        unit.cost = costs[1];
        unit.power = 0.35;
        unit.move = 3;
        unit.pv = 100;
        unit.range = 3;
        unit.type = 1;
    }
    else if(type == 2){
        unit.cost = costs[2];
        unit.power = 0.5;
        unit.move = 2;
        unit.pv = 100;
        unit.range = 6;
        unit.type = 2;
    }
    else if(type == 4){
        // Un obstacle de type transparent
        unit.cost = 0;
        unit.power = 0;
        unit.move = 0;
        unit.pv = 20000;
        unit.range = 0;
        unit.type = 19;
    }
    else if(type == 5){
        // Coffre au trésor
        unit.cost = 1000;
        unit.power = 0;
        unit.move = 0;
        unit.pv = 1;
        unit.range = 0;
        unit.type = 5;
    }
    else{
        // Un obstacle de type mur
        unit.cost = 0;
        unit.power = 0.2;
        unit.move = 0;
        unit.pv = 50;
        unit.range = 0;
        unit.type = type;
    }
    money[owner] -= unit.cost;
    return unit;
}
