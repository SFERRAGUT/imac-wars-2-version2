#pragma once
#include <iostream>
#include "game.h"
#include "map.h"
#include <list>
#include <map>
#include "math.h"
#include <utility>

using namespace std;

extern Unit* allUnits;
extern int unitCount;


struct noeud{
    float cout_g, cout_h, cout_f;
    pair<int,int> parent;
};

typedef struct point{
    int x,y;
}point;

typedef map< pair<int,int>, noeud> l_noeud;

float distance(int, int, int, int);
void ajouter_cases_adjacentes(pair<int,int>&);
bool deja_present_dans_liste( pair<int,int>,l_noeud&);
pair<int,int> meilleur_noeud(l_noeud&);
void ajouter_liste_fermee(pair<int,int>&);

void colorerPixel(int, int, Uint32);
void retrouver_chemin();
bool checkCell(int x, int y);

void pathFinder(point dep, point arrivee);


