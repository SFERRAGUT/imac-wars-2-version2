#include "display.h"
#include <iostream>
#include <cmath>
using namespace std;

extern bool isRunning;

int main(){
    cout << "Bienvenue dans IMAC WARS 2. Jeu developpe par Theo DAUPHIN, Simon FERRAGUT et Sterenn FONSECA." << endl;
	display_init();
    while (isRunning){
        display_update();
    }
}

