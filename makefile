#https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
#$^ contient les variables du dessus

all: game

game: main.o game.o unit.o map.o display.o ia.o astar.o
	gcc $^ -o game -lstdc++ -lSDL -lSDL_image -lSDL_ttf -g -lm
	@echo Compilation finie.

clean: 
	rm -f game *.o

%.o: %.cpp %.h
	gcc -c -Wall -lstdc++ -lSDL -lSDL_image -lSDL_ttf -g -lm $<




