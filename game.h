#pragma once
#include "SDL/SDL.h"
#include "map.h"
#include "display.h"
#include "ia.h"
#include "astar.h"

const int NB_PLAYERS = 2;

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int FPS = 60;
const int frameDelay = 1000 / FPS; //Durée d'une frame
const int margeleft = SCREEN_WIDTH/40;
const int margeright = (SCREEN_WIDTH+MAP_WIDTH*LARGEUR_TILE)/2 + margeleft;
const int margebottom = SCREEN_HEIGHT*(1-1/20);
const int margetop = SCREEN_HEIGHT/20;


void init_IA(int currentPlayer);
void game_IA(int currentPlayer);
void buyUnit_IA(int currentPlayer);
void action(Unit *unit, int destX, int destY, int currentPlayer);
void gameHandleEvent();
void changePlayer(Unit* unit, Unit* allUnits);
